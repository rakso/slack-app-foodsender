<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Get all of required files
require('variables.php');
require('libraries/foodParser.php');
require('libraries/slackApi.php');
require('libraries/filesManagment.php');

// Get the data from page
$foodParser = new FoodParser('http://pasibrzuch-szczecin.pl/?menu-i-cennik');
$foodParser->scrapData();

// Prepare array for file save resposne (for debug)
$storeDataState = array();

// Store info about dinners in file
$foodSenderFiles = new FoodSenderFiles($currentWorkingDir);
$storeDataState['dinner'] = $foodSenderFiles->storeDinnerData($foodParser->getScrappedSoupsAndDinners());

$slackApi = new SlackAPI($slackBotUserOAuthAccessToken);

// Prepare for sending messages to food channel
$slackMessage = new SlackMessage();
$slackMessage->setChannel($slackChannelId);

// Get scrapped data and send it as message to slack
$mealsOfTheDay = $foodParser->getScrappedSoupAndDinnersTextForCurrentDay();
$slackMessage->setMessage($mealsOfTheDay . $additionalMessageText);
$mealsOfTheDayResposne = $slackApi->sendMessage($slackMessage->message);

// Send also an advertisement of one of our friends restaurant
// $slackMessage->setMessage($pecorinoText);
// $pecorinoResposne = $slackApi->sendMessage($slackMessage->message);

// Is our main message was sent successfully then store it's message timestamp
// in file for futher usage it to make replies in thread
if ($mealsOfTheDayResposne !== false) {
    $responseAsObject = json_decode($mealsOfTheDayResposne);
    $messageCurrentTs = $responseAsObject->ts;
    $storeDataState['ts'] = $foodSenderFiles->storeCurrentTs($messageCurrentTs);
} else {
    // If there was an error store 0 as content
    $storeDataState['ts'] = $foodSenderFiles->storeCurrentTs(0);
}

// Give some info
echo json_encode($storeDataState);
echo $mealsOfTheDayResposne;
// echo $mealsOfTheDayResposne . '<br>' . $pecorinoResposne;
