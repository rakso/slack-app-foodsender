<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

cors();
header('Content-Type: application/json');

$entityBody = file_get_contents('php://input');
$request = json_decode($entityBody);

switch ($request->func) {
    case 'reply':
        echo sendReplyToThread($request->message);
        break;

    case 'getDinners':
        echo getDinnersData();
        break;

    default:
        echo 'BAD REQUEST! func & message';
        break;
}


function sendReplyToThread($replyMessage)
{
    // Get all of required files
    require('variables.php');
    require('libraries/slackApi.php');
    require('libraries/filesManagment.php');

    $foodSenderFiles = new FoodSenderFiles($currentWorkingDir);
    $mainMessageTs = $foodSenderFiles->getCurrentTs();

    if ($mainMessageTs === 0) {
        echo "Can't sent to thread - currentTs.txt == 0";
        return 0;
    }

    $slackMessage = new SlackMessage();
    $slackMessage->setChannel($slackChannelId);
    $slackMessage->setMessage($replyMessage);
    $slackMessage->asReplyTo($mainMessageTs);

    $slackApi = new SlackAPI($slackBotUserOAuthAccessToken);
    $messageSent = $slackApi->sendMessage($slackMessage->message);

    return $messageSent;
}

function getDinnersData()
{
    // Get all of required files
    require('variables.php');
    require('libraries/filesManagment.php');
    $foodSenderFiles = new FoodSenderFiles($currentWorkingDir);
    return $foodSenderFiles->getDinnerDataAsJson();
}

/**
 *  An example CORS-compliant method.  It will allow any GET, POST, or OPTIONS requests from any
 *  origin.
 *
 *  In a production environment, you probably want to be more restrictive, but this gives you
 *  the general idea of what is involved.  For the nitty-gritty low-down, read:
 *
 *  - https://developer.mozilla.org/en/HTTP_access_control
 *  - https://fetch.spec.whatwg.org/#http-cors-protocol
 *
 */
function cors()
{

    // Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
        // you want to allow, and if so:
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            // may also be using PUT, PATCH, HEAD etc
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }
}
