# Slack App - FoodSender

Script written in PHP which scrape the web page of our food supplier menu and send its parsed content to the to the specified channel on slack

- [Our food supplier](http://pasibrzuch-szczecin.pl/?ogolnie) ;)

### Info

To get started you will need to obtain `Bot User OAuth Access Token` and slack channel ID
