<?php

class FoodSenderFiles
{

    private $filesNames;

    public function __construct($currentWorkingDir)
    {
        $this->filesNames = array(
            'CURRENT_TS' => "$currentWorkingDir/files/currentTs.txt",
            'ACTUAL_DINNER_DATA' => "$currentWorkingDir/files/dinnerData.json"
        );
    }

    public function storeCurrentTs($dataToStore)
    {
        return file_put_contents($this->filesNames['CURRENT_TS'], $dataToStore);
    }

    public function getCurrentTs()
    {
        return file_get_contents($this->filesNames['CURRENT_TS']);
    }

    public function storeDinnerData($dataToStore)
    {
        return file_put_contents($this->filesNames['ACTUAL_DINNER_DATA'], json_encode($dataToStore));
    }

    public function getDinnerDataAsJson()
    {
        return file_get_contents($this->filesNames['ACTUAL_DINNER_DATA']);
    }

    public function getDinnerData()
    {
        return json_decode($this->getDinnerDataAsJson());
    }
}

// // Sample usage
// $foodSenderFiles = new FoodSenderFiles($currentWorkingDir);
// echo $foodSenderFiles->storeCurrentTs('XD');
// echo $foodSenderFiles->storeDinnerData(array('ok' => 3348723, "nok" => 'xD'));

// echo $foodSenderFiles->getCurrentTs();
// var_dump($foodSenderFiles->getDinnerData());
