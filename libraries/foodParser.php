<?php
require('simpleDomParser/simple_html_dom.php');

class FoodParser
{
    private $html;
    private $weekMealsList;
    private $webpageUrl;

    public function __construct($webpageUrl)
    {
        $this->webpageUrl = $webpageUrl;
    }

    private function downloadWebpage()
    {
        $this->html = file_get_html($this->webpageUrl);
        return $this;
    }

    private function parseHtmlForWeekMenu()
    {
        $this->weekMealsList = $this->html->find('.pages-list')[0];
        return $this;
    }

    private function getMenuForDay($weekDay)
    {
        return $this->weekMealsList->find('li')[$weekDay];
    }

    private function getMenuForTheCurrentDay()
    {
        return $this->getMenuForDay(date('w') - 1);
    }

    private function parseDinnnersAndSoupsForCurrentDay()
    {
        $dinnersBlock = $this->getMenuForTheCurrentDay()->find('div.description')[0];
        $dinnersRawArray = $dinnersBlock->find('p');

        // Soups are at the first row
        $soupsRawObject = $dinnersRawArray[0];
        // That "soup text" looks like "  Dziś zupa:      koperkowa           rosół z makaronem "
        // We want only soups!
        $justSoups = str_ireplace('Dziś zupa:', '', $soupsRawObject->plaintext);
        // Sometimes it is "Dziś zupa: " so wee need to clean up both
        $justSoups = str_ireplace('Dziś zupa: ', '', $soupsRawObject->plaintext);
        // These texts are delimeted by &nbsp;, let's split them
        $soupsRawArray = explode("\xc2\xa0", $justSoups);

        $soupsArray = array();
        foreach ($soupsRawArray as $key => $soupRawText) {
            // We have an array full of empty strings (text &nbsp;&nbsp; splited by &nbsp;) and we need only
            // usable (length > 2) text
            if (strlen($soupRawText) > 2)
                // We can clean a little bit our text
                array_push($soupsArray, trim($soupRawText, "\xc2\xa0 "));
        }

        $dinnersArray = array();
        // We don't need the first line of text (there is soup info)
        array_splice($dinnersRawArray, 0, 1);
        foreach ($dinnersRawArray as $dinnerNameObject) {
            // Let's clean our data
            $dinnerName = trim($dinnerNameObject->plaintext, "\xc2\xa0 ");
            // And fetch only usable text
            if (strlen($dinnerName) > 2)
                array_push($dinnersArray, $dinnerName);
        }

        return array('soups' => $soupsArray, 'dinners' => $dinnersArray);
    }

    public function scrapData()
    {
        $this->downloadWebpage()->parseHtmlForWeekMenu();
    }

    public function getScrappedSoupAndDinnersTextForCurrentDay()
    {
        return $this->getMenuForTheCurrentDay()->plaintext;
    }

    public function  getScrappedSoupsAndDinners()
    {
        return $this->parseDinnnersAndSoupsForCurrentDay();
    }
}

// // Sample usage
// $foodParser = new FoodParser('http://pasibrzuch-szczecin.pl/?obiady');
// $foodParser->scrapData();
// // echo json_encode($foodParser->getScrappedSoupsAndDinners());
// echo $foodParser->getScrappedSoupAndDinnersTextForCurrentDay();
