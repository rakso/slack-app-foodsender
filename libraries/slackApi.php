<?php

class SlackAPI
{
    private $token;

    public function __construct($slackBotUserOAuthAccessToken)
    {
        $this->token = $slackBotUserOAuthAccessToken;
    }

    private function makeRequest($apiEndpoint, $postData)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://slack.com/api/$apiEndpoint",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postData,
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer $this->token",
                "content-type: application/json; charset=utf-8"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
            return false;
        } else {
            return $response;
        }
    }

    public function sendMessage($message)
    {
        $apiEndpoint = 'chat.postMessage';
        return $this->makeRequest($apiEndpoint, $message);
    }
}

class SlackMessage
{
    private $messageObject = array();
    public function setChannel($channelId)
    {
        $this->messageObject['channel'] = $channelId;
    }

    public function setMessage($message)
    {
        $this->messageObject['text'] = $message;
    }

    // shouldBeBroadcasted - If set to true, then message will be also sent to channel
    public function asReplyTo($mainMessageTs, $shouldBeBroadcasted = FALSE)
    {
        $this->messageObject['thread_ts'] = $mainMessageTs;
        if ($shouldBeBroadcasted)
            $this->messageObject['reply_broadcast'] = TRUE;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'message':
                return json_encode($this->messageObject);
        }
    }
}

// // Sample usage
// $slackMessage = new SlackMessage();
// $slackMessage->setChannel('G0194SQPSN7');
// $slackMessage->setMessage('TEEEST');
// $slackMessage->asReplyTo('1610493651.000300', true);

// $slackApi = new SlackAPI('xoxb-');
// echo $slackApi->sendMessage($slackMessage->message);
